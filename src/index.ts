import '@babel/polyfill';
import { define, props, withComponent } from 'skatejs';
import withLitHtml from '@skatejs/renderer-lit-html';
import { html } from 'lit-html';

import { fetchSummary } from './util/fetch_status';
import { Summary } from './types/summary';
import AVAILABLE_TEMPLATES from './templates';

export interface Props {
  src: string;
  appearance: string;
  title: string;
}

interface State {
  summary: Summary | null;
}

@define
class StatuspageWidget extends withComponent<Props>(withLitHtml()) {
  static is = 'statuspage-widget';

  static get props() {
    return {
      src: props.string,
      appearance: props.string,
      title: props.string,
    };
  }

  state: State = {
    summary: null,
  };

  connected() {
    fetchSummary(this.props.src).then(result => {
      this.state.summary = result;
      this.updated();
    });
  }

  render({ props, state }: { props: Props; state: State }) {
    const template =
      AVAILABLE_TEMPLATES[this.props.appearance] ||
      AVAILABLE_TEMPLATES['basic'];

    if (this.state.summary !== null) {
      return template(this.state.summary, this.props);
    }
    return html`
      <style>
        :host {
          display: inline-block;
        }
      </style>
      <div></div>
    `;
  }
}
