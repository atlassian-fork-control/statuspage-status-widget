import { TemplateResult } from 'lit-html';

import { Summary } from '../types/summary';

import basic from './basic';
import badge from './badge';
import { Props } from '..';

interface TemplateList {
  [key: string]: (summary: Summary, props: Props) => TemplateResult;
}

const AVAILABLE_TEMPLATES: TemplateList = {
  basic,
  badge,
};

export default AVAILABLE_TEMPLATES;
