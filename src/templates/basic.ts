import { html } from 'lit-html';
import { Summary } from '../types/summary';
import { Props } from '..';

export default function template(summary: Summary, props: Props) {
  return html`
    <style>
      :host {
        display: inline-block;
      }

      a.basic,
      a.basic:hover,
      a.basic:hover {
        color: inherit;
        text-decoration: none;
      }

      .basic .status-container {
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
          'Oxygen', 'Ubuntu', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
          sans-serif;
        display: flex;
        align-items: center;
        height: 1.5rem;
        padding: 0.25rem;
      }

      .basic .status-indicator {
        width: 1rem;
        height: 1rem;
        border-radius: 50%;
        background-color: #aaa;
        line-height: 1rem;
      }

      .basic .none {
        background-color: #2ecc71;
      }
      .basic .operational {
        background-color: #2ecc71;
      }
      .basic .minor {
        background-color: #f1c40f;
      }
      .basic .major {
        background-color: #e67e22;
      }
      .basic .critical {
        background-color: #e74c3c;
      }
      .basic .maintenance {
        background-color: #3498db;
      }

      .basic .status-text {
        margin-left: 0.5rem;
        line-height: 1rem;
      }
    </style>

    <a href="${props.src}" class="basic" target="_blank">
      <div class="status-container">
        <div class="status-indicator ${summary.status.indicator}"></div>
        <div class="status-text">${summary.status.description}</div>
      </div>
    </a>
  `;
}
